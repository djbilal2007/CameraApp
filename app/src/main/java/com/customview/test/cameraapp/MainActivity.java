package com.customview.test.cameraapp;

import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PICK_IMAGE_REQUEST = 1;
    private static final int CAMERA_REQUEST = 202;

    private Button choose_btn, capture_btn, share_btn;
    private ImageView imageView;

    private static Uri filePath;
    private MarshMallowPermission marshMallowPermission;

    private String currentPhotoPath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        marshMallowPermission = new MarshMallowPermission(this);

        choose_btn = (Button)findViewById(R.id.choose_img_btn);
        capture_btn = (Button)findViewById(R.id.capture_img_btn);
        share_btn = (Button)findViewById(R.id.share_btn);
        imageView = (ImageView)findViewById(R.id.imageView);

        choose_btn.setOnClickListener(this);
        capture_btn.setOnClickListener(this);
        share_btn.setOnClickListener(this);

    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select an Image"), PICK_IMAGE_REQUEST);
    }

    private void captureImage() {
        if(!marshMallowPermission.checkPermissionForCamera()){
            marshMallowPermission.requestPermissionForCamera();
        }else {
            if(!marshMallowPermission.checkPermissionForExternalStorage()){
                marshMallowPermission.requestPermissionForExternalStorage();
            }else {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File file = null;
                try {
                        file = createImageFile();
                } catch (IOException e) {
                        e.printStackTrace();
                }
                if (file != null) {
                    Log.d("File","File is not null");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(intent, CAMERA_REQUEST);
                }
            }
        }
    }

    private File createImageFile() throws IOException{
        String imageFileName = "IMG_" + System.currentTimeMillis();
        File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDirectory);

        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case MarshMallowPermission.CAMERA_PERMISSION_REQUEST_CODE:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    captureImage();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            filePath = data.getData();
            currentPhotoPath = getRealPathFromUri(filePath);
            try {
                //Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath);
                imageView.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if(requestCode == CAMERA_REQUEST && resultCode == RESULT_OK){
            try {
                Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath);

               // Bitmap bitmap2 = ScreenRotation.handleRotatingBitmap(this, Uri.fromFile(new File(currentPhotoPath)));

                imageView.setImageBitmap(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void shareImage() {
        Log.d("FilePath", currentPhotoPath);

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        File file = new File(currentPhotoPath);
        Uri uri = Uri.fromFile(file);
        share.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(share, "Share to"));
    }

    private String getRealPathFromUri(Uri contentUri) {
        String [] proj = {MediaStore.Images.Media.DATA};
        CursorLoader cursorLoader = new CursorLoader(this, contentUri, proj, null, null,null);
        Cursor cursor = cursorLoader.loadInBackground();
        int colmn_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(colmn_index);
        cursor.close();
        return result;
    }

    @Override
    public void onClick(View v) {
        if (v == choose_btn){
            chooseImage();
        }
        if (v == capture_btn){
            captureImage();
        }
        if (v == share_btn){
            shareImage();
        }
    }
}
