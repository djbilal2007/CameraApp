package com.customview.test.cameraapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

public class ScreenRotation {

    public static Bitmap handleRotatingBitmap(Context context, Uri image) throws IOException{
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        InputStream imageStream = context.getContentResolver().openInputStream(image);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        options.inJustDecodeBounds = false;

        imageStream = context.getContentResolver().openInputStream(image);
        Bitmap bitmap = BitmapFactory.decodeStream(imageStream, null, options);

        bitmap = rotateImageIfRequired(bitmap, image);
        return bitmap;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int max_width, int max_height) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if(height > max_height || width > max_width){
            final int height_ratio = Math.round((float)height/(float)max_height);
            final int width_ratio = Math.round((float)width/(float)max_height);

            inSampleSize = height_ratio < width_ratio ? height_ratio : width_ratio;

            final float totalPixels = width * height;

            final float totalMaxPixelsCap = max_height * max_width * 2;

            while(totalPixels / (inSampleSize * inSampleSize) > totalMaxPixelsCap){
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private static Bitmap rotateImageIfRequired(Bitmap bitmap, Uri image) throws IOException{

        ExifInterface exif = new ExifInterface(image.getPath());
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        Log.d("Orientation",""+orientation);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                Log.d("Orientation","90");
                return rotateImage(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                Log.d("Orientation","180");
                return rotateImage(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                Log.d("Orientation","270");
                return rotateImage(bitmap, 270);

            default:
                return bitmap;
        }
    }

    private static Bitmap rotateImage(Bitmap bitmap, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImage = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        //bitmap.recycle();
        return rotatedImage;
    }


}
